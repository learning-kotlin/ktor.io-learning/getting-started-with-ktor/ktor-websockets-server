package org.elu.learn.ktor

import io.ktor.server.application.*
import org.elu.learn.ktor.plugins.*

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    configureSockets()
    configureRouting()
}
